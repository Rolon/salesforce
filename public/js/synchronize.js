let listener = setInterval("listenerContactos()", 5000);

function listenerContactos() {
    try {
        /* Primero verifica si existen los datos del usuario */
        let urlListen = "/listenerExportContacts";

        const metas = document.getElementsByTagName('meta');

        let metaToken = "";

        for (let i = 0; i < metas.length; i++) {
            if (metas[i].getAttribute('name') == "csrf-token") {
                metaToken = metas[i].getAttribute('content');
            }
        }

        fetch(urlListen, {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json, text-plain, */*",
                    "X-Requested-With": "XMLHttpRequest",
                    'X-CSRF-Token': metaToken
                },
                method: 'post',
                credentials: "same-origin",
                body: JSON.stringify({
                    export: true
                })
            })
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                // if success == false then put a notification error about salesforce connection
                let success = data.success;
                let html = "";
                var notification = document.getElementById("notification");
                if (!success) {
                    html = '<div class="col"><div id="errorAlert" class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Error!</strong> Usted no se encuentra conectado con Salesforce, por favor realice la autenticación inmediatamente<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div></div>';
                    notification.innerHTML = html;
                    console.log(data);
                    // Stops listener after close notification
                    $('#errorAlert').on('closed.bs.alert', function () {
                        clearInterval(listener);
                    });
                }else{
                    html = '<div class="col"><div id="errorAlert" class="alert alert-success alert-dismissible fade show" role="alert"><strong>Éxito!</strong> Los contactos han sido exporatdos<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div></div>';
                    notification.innerHTML = html;
                    $('#errorAlert').on('closed.bs.alert', function () {
                        $("#errorAlert").alert('close');
                    });
                }
            });
    } catch (error) {
        console.log(error);
    }
}
