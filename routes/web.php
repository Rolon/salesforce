<?php
/*

|--------------------------------------------------------------------------

| Web Routes

|--------------------------------------------------------------------------

|

| Here is where you can register web routes for your application. These

| routes are loaded by the RouteServiceProvider within a group which

| contains the "web" middleware group. Now create something great!

|

*/

// All routes are content in middleware auth
Route::group(['middleware' => 'auth'],function (){

/* ---------------------------------------------------------------------------*/

// Route root

Route::get('/', function () {
    return view('welcome');
});

/* ---------------------------------------------------------------------------*/

// Auth for salesforce

Route::get('/authenticate', function(){ // Route Auth token
    return Forrest::authenticate();
});

Route::get('/callback', function(){ // Route callback token
    Forrest::callback();
    return Redirect::to('/config');
});

Route::get('/revoke',function(){ // Route revoke token
    try{
        Forrest::revoke();
    }catch(\Exception $ex){
        return Redirect::to('/config');
    }
    return Redirect::to('/config');
});

Route::get('/refresh',function(){ // Route refresh token
    Forrest::refresh();
    return Redirect::to('/config');
});

/* ---------------------------------------------------------------------------*/

// Retrieves the view for create new contact
Route::get('/contactoNV','Contactos@createContactView');

Route::post('/createC','Contactos@createContact');

/* ---------------------------------------------------------------------------*/

// Route to set variables for salesforce login

Route::get('/config','Contactos@salesforceconfigView');

// Config salesforce login
Route::post('/salesforceConfigByUser','Contactos@salesforceConfigByUser');

Route::post('/salesforceConfigByToken','Contactos@salesforceConfigByToken');

// Refresh token by user or webtoken
Route::get('/refreshToken','Contactos@refreshByToken');

Route::get('/refreshUser','Contactos@refreshByUser');

/* ---------------------------------------------------------------------------*/

// Route to create activity for a contact
Route::get('/ActividadNV','Contactos@viewActividad');

Route::post('/createA','Contactos@actividad');

/* ---------------------------------------------------------------------------*/

// Create new device view
Route::get('/DispositivoNV','Contactos@viewDevice');

Route::post('/createD','Contactos@device');

/* ---------------------------------------------------------------------------*/

// Route to modify contacts view
Route::get('/modificarContacto','Contactos@viewModify');

// Route to modify a contact view
Route::get('/modificarContacto/{id}','Contactos@viewModifyContact');

// Route to modify a contact
Route::patch('/modificarC/{id}','Contactos@ModifyContact');

/* ---------------------------------------------------------------------------*/

// Veroficar conexión
Route::post('/verifyData','Contactos@verifyData');

Route::post('/verifyConnection','Contactos@verifyConnection');

/* ---------------------------------------------------------------------------*/

// Listener para escuchar contactos
Route::post('/listenerExportContacts','Contactos@listenerExportContacts');

/* ---------------------------------------------------------------------------*/

// Json para el Bot de facebook

Route::get('/bot','Contactos@contact_total');

/* ---------------------------------------------------------------------------*/
// Route to home

Route::get('/home', 'HomeController@index')->name('welcome');

});

// Routes for auth
Auth::routes();


