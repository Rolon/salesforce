<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table -> bigIncrements('id');
            $table -> string('firstName', 100);
            $table -> string('lastName', 100);
            $table -> string('pathPhoto', 100);
            $table -> string('latitude', 100);
            $table -> string('length', 100);
            $table -> string('device', 100);
            $table -> string('number', 100);
            $table -> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
