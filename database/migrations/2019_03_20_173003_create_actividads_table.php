<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActividadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividads', function (Blueprint $table) {
            $table->bigIncrements('id');           
            $table->string('descripcion',255);
            $table->string('foto_actividad',255);
            $table->string('latitude',255);
            $table->string('longitude',255);
            $table->string('dispositivo',255);
            $table->unsignedBigInteger('contacto_id')->unsigned();
            $table->unsignedBigInteger('dispositivo_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actividads');
    }
}
