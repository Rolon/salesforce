<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesforcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salesforces', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('consumer_key',255);
            $table->string('consumer_secret',255);
            $table->string('callback_uri',255);
            $table->string('login_url',255);
            $table->string('username',255);
            $table->string('password',255);
            $table->unsignedBigInteger('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salesforces');
    }
}
