<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dispositivo extends Model
{
    // Many to One user
    public function user(){
        return $this -> belongsTo("App\User");
    }

    // One to many activities
    public function actividades(){
        return $this -> hasMany("App\actividad");
    }
}
