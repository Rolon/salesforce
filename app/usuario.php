<?php

namespace App;

// Class Model
use Illuminate\Database\Eloquent\Model;

// Clase request
use Illuminate\Http\Request;

// Clase DB
use Illuminate\Support\Facades\DB;

// Clase Storage
use Illuminate\Support\Facades\Storage;

// Clase Input
use Illuminate\Support\Facades\Input;

class usuario extends Model
{
    // Table
    protected $table = "usuarios";
    
    // Primary Key
    protected $primaryKey = 'id';

    // Inputs fillables
    protected $fillable = [
        "firstName",
        "lastName",
        "pathPhoto",
        "latitude",
        "length",
        "device",
        "number"
    ];

    // Gets all users with all items within
    public function getUsers(){
        $usuarios = usuario::all() -> get();
        return $usuarios;
    } 

    // Create
    public static function setNewUser(Request $request){        
        //Variable de retorno de tipo boolean
        $exito = false;

        // get variables from request
        $firstName = $request -> get("firstName");
        $lastName = $request -> get("lastName");
        $latitude = $request -> get("latitude");
        $length = $request -> get("length");
        $device = $request -> get("device");
        $number = $request -> get("number");

        try{
            DB::beginTransaction();
                $usuario = new usuario();

                $usuario -> firstName = $firstName;
                $usuario -> lastName = $lastName;                
                $usuario -> latitude = $latitude;
                $usuario -> length = $length;
                $usuario -> device = $device;
                $usuario -> number = $number;

                // Store photo
                $lastUserId = usuario::all() -> last() -> id;
                $photoName = "photo_".$lastUserId;
                $usuario -> pathPhoto = $request -> file('pathPhoto') -> storeAs('/public', $photoName);

                $usuario -> save();

            DB::commit();

            $exito = true;
        }catch(Exception $ex){
            DB::rollback();
            $exito = false;
        }

        return $exito;
    }

}
