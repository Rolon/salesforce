<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class salesforce extends Model
{

    // Many to One user
    public function user(){
        return $this -> belongsTo("App\User");
    }
}
