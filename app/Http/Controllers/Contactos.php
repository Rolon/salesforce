<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Config
use Config;

//Modelo contacto
use App\contacto;

//Modelo actividad
use App\actividad;

//Modelo salesforce
use App\salesforce;

//Modelo contacto
use App\User;

//Modelo dispositivo
use App\dispositivo;

//Clase DB
use Illuminate\Support\Facades\DB;

// Forrest Class
use Omniphx\Forrest\Providers\Laravel\Facades\Forrest;

//Auth class
use Illuminate\Support\Facades\Auth;



class Contactos extends Controller
{
    // Retrives the view for salesforce login
    public function salesforceconfigView(){
        // User
        $user = Auth::user();

        // Cuenta salesforce 
        $salesforce = User::find($user -> id) -> salesforces;

        $values =[];

        if(count($salesforce) > 0){
            // Creates an array with parameters from env file
            $values = [
                "user" => $salesforce[0] -> username,
                "pass" => $salesforce[0] -> password,
                "key" => $salesforce[0] -> consumer_key,
                "secret" => $salesforce[0] -> consumer_secret
            ];
        }else{
            $values = [
                "user" => "",
                "pass" => "",
                "key" => "",
                "secret" => ""
            ];
        }

        $json_env = json_encode($values);
        $json_env = json_decode($json_env);
        
        return view('contactos.salesforceConfig')
        ->with("json_env" ,$json_env)
        ;
    }

    // Configures the salesforce .evn variables user
    public function salesforceConfigByUser(Request $request){
        try{
            // User
            $user = Auth::user();

            // Cuenta salesforce 
            $salesforce = User::find($user -> id) -> salesforces;

            if(count($salesforce) == 0){
                $salesforce = new salesforce();
                $salesforce -> user_id = $user -> id;
                
                $salesforce -> callback_uri = "";            
            }else{
                $salesforce = salesforce::where('user_id','=',$user -> id)->firstOrFail();
            }

            $salesforce -> consumer_key = $request -> keyOne;
            $salesforce -> consumer_secret = $request -> keyTwo;
            $salesforce -> username = $request -> user;
            $salesforce -> password = $request -> pass;

            $salesforce -> save();

            // Login for Token
            config(['forrest.authentication' => 'UserPassword']); // Auth Type
            config(['forrest.credentials.consumerKey' => $request -> keyOne]); // Consummer Key
            config(['forrest.credentials.consumerSecret' => $request -> keyTwo]); // Consummer Secret
            config(['forrest.credentials.username' => $request -> user]); // UserName
            config(['forrest.credentials.password' => $request -> pass]); // Password
            config(['forrest.credentials.loginURL' => $salesforce -> login_url]); // URL login
            
            Forrest::authenticate();
            return redirect('/config');
        }catch(\Exception $ex){
            return view('errors.salesforce');
        }
    }

    // Configures the salesforce .evn variables token
    public function salesforceConfigByToken(Request $request){
        try{
            // User
            $user = Auth::user();

            // Cuenta salesforce 
            $salesforce = User::find($user -> id) -> salesforces;

            if(count($salesforce) == 0){
                $salesforce = new salesforce();
                $salesforce -> user_id = $user -> id;
                
                $salesforce -> username = "";
                $salesforce -> password = "";
            }else{
                $salesforce = salesforce::where('user_id','=',$user -> id)->firstOrFail();
            }

            $salesforce -> consumer_key = $request -> keyOne;
            $salesforce -> consumer_secret = $request -> keyTwo;

            $salesforce -> save();

            // Login by Token
            config(['forrest.authentication' => 'WebServer']); // Auth Type
            config(['forrest.credentials.consumerKey' => $request -> keyOne]); // Consummer Key
            config(['forrest.credentials.consumerSecret' => $request -> keyTwo]); // Consummer Secret
            config(['forrest.credentials.callbackURI' => $salesforce -> callback_uri]); // Callback URI
            config(['forrest.credentials.loginURL' => $salesforce -> login_url]); // Login URL

            return Forrest::authenticate();
        }catch(\Exception $ex){
            return view('errors.salesforce');
        }
    }

    // Refresh user by userpassword
    public function refreshByUser(){
        try{
            // User
            $user = Auth::user();

            // Cuenta salesforce 
            $salesforce = User::find($user -> id) -> salesforces;

            // Login for userpassword
            config(['forrest.authentication' => 'UserPassword']); // Auth Type
            config(['forrest.credentials.consumerKey' => $salesforce[0] -> consumer_key]); // Consummer Key
            config(['forrest.credentials.consumerSecret' => $salesforce[0] -> consumer_secret]); // Consummer Secret
            config(['forrest.credentials.username' => $salesforce[0] -> username]); // UserName
            config(['forrest.credentials.password' => $salesforce[0] -> password]); // Password
            config(['forrest.credentials.loginURL' => $salesforce[0] -> login_url]); // URL login
            
            Forrest::authenticate();
            return redirect('/config');
        }catch(\Exception $ex){
            return view('errors.salesforce');
        }
    }

    // Refresh user by userpassword
    public function refreshByToken(){
        try{
            // User
            $user = Auth::user();

            // Cuenta salesforce 
            $salesforce = User::find($user -> id) -> salesforces;

            // Login by Token
            config(['forrest.authentication' => 'WebServer']); // Auth Type
            config(['forrest.credentials.consumerKey' => $salesforce[0] -> consumer_key]); // Consummer Key
            config(['forrest.credentials.consumerSecret' => $salesforce[0] -> consumer_secret]); // Consummer Secret
            config(['forrest.credentials.callbackURI' => $salesforce[0] -> callback_uri]); // Callback URI
            config(['forrest.credentials.loginURL' => $salesforce[0] -> login_url]); // Login URL

            return Forrest::authenticate();
        }catch(\Exception $ex){
            return view('errors.salesforce');
        }
    }

    // Retrieves the view for create a new user
    public function createContactView(){
        return view('contactos.nuevoContacto');
    }

    // Creates new contact
    public function createContact(Request $request){
        // User
        $user = Auth::user();

        // contacto
        $contacto = new contacto();
        if(isset($request -> name)){
            $contacto -> name = $request -> name;
        }
        
        if(isset($request -> lastName)){
            $contacto -> lastName = $request -> lastName;
        }

        if(isset($request -> mail)){
            $contacto -> correo = $request -> mail;
        }

        if(isset($request -> genero)){
            $contacto -> genero = $request -> genero;
        }

        if(isset($request -> mobile)){
            $contacto -> mobile = $request -> mobile;
        }

        $contacto -> user_id = $user -> id;
        
        $contacto -> save();

        return redirect('/');
    }

    // View for create a new activity for a contact
    public function viewActividad(){
        $user = Auth::user();

        $dispositivos = User::find($user -> id) -> dispositivos;
        $contactos = User::find($user -> id) -> contactos;

        return view('contactos.nuevaActividad')
        ->with('dispositivos',$dispositivos)
        ->with('contactos', $contactos)
        ;
    }

    // Crear actividad
    public function actividad(Request $request){
        // save activity into database
        // actividad
        $actividad = new actividad();

        $actividad -> descripcion = $request -> description;
        $actividad -> georeferencia = $request -> georeference;
        $actividad -> dispositivo = "android";
        $actividad -> contacto_id = $request -> contacto;
        $actividad -> dispositivo_id = $request -> device;
        $fotoreturn = explode('/',$request->file('photo')->store('/public'));
        $actividad -> foto_actividad = $fotoreturn[1]; 
        $actividad -> exported = 1;

        $actividad -> save();

        // Datos from user
        $contacto = contacto::findOrFail($request -> contacto);

        $dispositivo = dispositivo::findOrFail($request -> device);


        $actividad_descripcion = "Foto: ".$request -> file('photo') -> getClientOriginalName()."\n Lugar: ".$request -> georeference."\n Actividad:".$request -> description."\n Dispositivo: ".$dispositivo -> device;

        // save activity into salesforce
        // Body for feed item
        $body = [
            "body" => [
                "messageSegments" => array([
                    "type" => "Text",
                    "text" => $actividad_descripcion
                ])
            ],
            "feedElementType" => "FeedItem",
            "subjectId" => $contacto -> id_exported
        ];

         Forrest::post('/services/data/v45.0/chatter/feed-elements/',$body);

        // body for images
        // image to blob
        $foto = file_get_contents($request -> file('photo'));
        $foto = base64_encode($foto);
            
        $body = [
            "ParentId" => $contacto -> id_exported,
            "Name" =>  $request -> file('photo') -> getClientOriginalName(),
            "body" => $foto
        ];

        Forrest::post('/services/data/v29.0/sobjects/Attachment/',$body);

        return redirect('/');
        
    }

    // Vista para crear nuevo device
    public function viewDevice(){
        return view('contactos.nuevoDevice');
    }

    // Crear un dispositivo nuevo
    public function device(Request $request){

        // device
        $device = new dispositivo();

        // User
        $user = Auth::user();

        $device -> device = $request -> nameD;
        $device -> user_id =  $user -> id;

        $device -> save();

        return redirect('/');
    }

    // Vista modificar contactos
    public function viewModify(){
        // User
        $user = Auth::user();

        // Cuenta salesforce 
        $contactos = User::find($user -> id) -> contactos;

        return view('contactos.modificarVista')
        ->with('contactos',$contactos)
        ;
    }

    // Vista modificar contactos
    public function viewModifyContact($id){
        $contacto = contacto::findOrFail($id);

        return view('contactos.modificarContacto')
        ->with('contacto',$contacto)
        ;
    }

    // Función para modificar un contacto local y de salesforce
    public function ModifyContact(Request $request, $id){
        $contacto = contacto::findOrFail($id);

        if($contacto -> name != $request -> name){
            $contacto -> name = $request -> name;                       
        }

        if($contacto -> lastName != $request -> lastName){
            $contacto -> lastName = $request -> lastName;
            $body['LastName'] = $request -> lastName;
        }

        if($contacto -> correo != $request -> mail){
            $contacto -> correo = $request -> mail;
            $body['Email'] = $request -> mail;
        }

        if($contacto -> genero != $request -> genero){
            $contacto -> genero = $request -> genero;
            if($request -> genero == "male"){
                $body['Salutation'] = "Mr.";
            }else if($request -> genero == "female"){
                $body['Salutation'] = "Ms.";
            }
        }

        if($contacto -> mobile != $request -> mobile){
            $contacto -> mobile = $request -> mobile;
            $body['MobilePhone'] = $request -> mobile;
        }

        $contacto -> save();

        if(isset($contacto -> id_exported)){
            $salutation = null;
            if($request -> genero == "male"){
               $salutation = "Mr.";
            }else if($request -> genero == "female"){
                $salutation = "Ms.";
            }

            $body = [
                'LastName' => $request -> lastName,
                'Email' => $request -> mail,
                'Salutation' => $salutation,
                'MobilePhone' => $request -> mobile,
                'FirstName' => $request -> name
            ];
            
            Forrest::sobjects('Contact/'.$contacto -> id_exported,[
                'method' => 'patch',
                'body'   => $body
            ]);
        }

        return redirect("/modificarContacto/".$contacto -> id);
    }


    // Verificar los datos del usuario 
    public function verifyData(Request $request){
        if($request -> ajax()){
            // Verificar que los datos de salesforce existan
            $user = Auth::user();

            $salesforce = User::find($user->id)->salesforces;

            if(isset($salesforce[0] -> consumer_key) && isset($salesforce[0] -> consumer_secret)){
                $json_return = [
                    "data_set" => true 
                ];
                return response() -> json($json_return);
            }

            $json_return = [
                "data_set" => false 
            ];

            return response() -> json($json_return);
        }
    }

    // Verificar la conexión con salesforce
    public function verifyConnection(Request $request){        
        if($request -> ajax()){
            try{
                $loggedIn = Forrest::identity();
                $loggedIn = json_encode($loggedIn);
                $loggedIn = json_decode($loggedIn);

                $json_response = [
                    "active" => $loggedIn -> active
                ];

                if($loggedIn -> active){
                    $json_response["active"] = $loggedIn -> active;

                    return response() -> json($json_response);
                }

                $json_response["active"] = False;

                return response() -> json($json_response);
            }catch(\Exception $ex){
                return response() -> json(["success" => False]);
            }
        }
    }
    
    // Función que es usada para exportar los contactos
    public function listenerExportContacts(Request $request){
        // Comment this line for online version
        // Uncomment this lone for local version
        set_time_limit(0); // This line allows long duration for the function
        if($request -> ajax()){
            try{
                // Verify Connection to salesforce
                $loggedIn = Forrest::identity();
                
                $loggedIn = json_encode($loggedIn);
                $loggedIn = json_decode($loggedIn);

                $salutation = "";

                // If salesforce is connected then exports contact
                if($loggedIn -> active){
                    // Find contacts
                    $user = Auth::user();

                    $contactos = User::find($user->id)->contactos->where("exported","=",0);

                    $countObjects = count($contactos);

                    // If exists contacts to export then exports them
                    if($countObjects > 0){

                        // Export contacts
                        foreach($contactos as $contacto){
    
                            if($contacto -> genero == "male"){
                                $salutation = "Mr.";
                            }else if($contacto -> genero == "female"){
                                $salutation = "Ms.";
                            }
                            
                            // Body for export contact
                            $body = [
                                'FirstName' => $contacto -> name,
                                'LastName' => $contacto -> lastName,
                                'Email' => $contacto -> correo,
                                'Salutation' => $salutation,
                                'MobilePhone' => $contacto -> mobile
                            ];

                            $contact_response = json_encode(Forrest::sobjects('Contact',[
                                'method' => 'post',
                                'body'   => $body
                            ]));
                
                            $contact_response = json_decode($contact_response);
                            
                            // If export were correct then updates database exported status
                            if($contact_response -> success == True){
                                try{
                    
                                    DB::beginTransaction();
                                        $contactoUpdate = contacto::where('id','=',$contacto->id)->firstOrFail();
                                        $contactoUpdate -> exported = "1";
                                        $contactoUpdate -> id_exported = $contact_response -> id;
                                        $contactoUpdate -> save();
                                    DB::commit();
                                    
                                }catch(\Exception $ex){
                                    
                                    DB::rollback();
                                    return response() 
                                    -> json([
                                        "success" => False,
                                        "message" => $ex -> getMessage()
                                    ]);
                                    
                                }
                            }
                        }
                    }

                    // Then exports all feed items
                    $actividades = actividad::all()->where("exported","=",0);

                    $countActividades = count($actividades);

                    // If exists activities then exports them
                    if($countActividades > 0){
                        foreach($actividades as $actividad){
                            // Salesforce contact id
                            $contacto = contacto::findOrfail($actividad -> contacto_id);

                            $dispositivo = dispositivo::findOrFail($actividad -> dispositivo_id);

                            $actividad_descripcion = "Foto: ".$actividad -> foto_actividad."\n Lugar: ".$actividad -> georeferencia."\n Actividad:".$actividad -> descripcion."\n Dispositivo: ".$dispositivo -> device;

                            // Body for feed item
                            $body = [
                                "body" => [
                                    "messageSegments" => array([
                                        "type" => "Text",
                                        "text" => $actividad_descripcion
                                    ])
                                ],
                                "feedElementType" => "FeedItem",
                                "subjectId" => $contacto -> id_exported
                            ];

                            // Export activity and get response
                            $actividad_response = Forrest::post('/services/data/v45.0/chatter/feed-elements/',$body);

                            // If exist photo then export them too
                            if(isset($actividad -> foto_actividad)){

                                // Convert file to base64 file blob data
                                $foto = file_get_contents("../storage/app/public/".$actividad -> foto_actividad);
                                $foto = base64_encode($foto);

                                // Body for export image
                                $body = [
                                    "ParentId" => $contacto -> id_exported,
                                    "Name" =>  $contacto -> id_exported."_".$actividad -> foto_actividad,
                                    "body" => $foto
                                ];

                                $foto_response = Forrest::post('/services/data/v29.0/sobjects/Attachment/',$body);

                                if($actividad_response["parent"]["id"] == $contacto -> id_exported && $foto_response["success"]){
                                    try{
                    
                                        DB::beginTransaction();
                                            $actividad -> exported = 1;
                                            $actividad -> save();
                                        DB::commit();
                                    
                                    }catch(\Exception $ex){
                                            
                                        DB::rollback();
                                        return response() 
                                        -> json([
                                            "success" => False,
                                            "message" => $ex -> getMessage()
                                        ]);
                                            
                                    }
                                }
                            }

                        }
                    }
                    
                    return response() -> json(["success" => True]);
                }

            }catch(\Exception $ex){
                return response() 
                -> json([
                    "success" => False,
                    "message" => $ex -> getMessage()
                ]);
            }
        }
    }


    // JSON for facebook bot
    function contact_total(){
        $user = Auth::user();

        $contactos = User::find($user->id)->contactos->where("exported","=",0);

        $contact_number = count($contactos);

        $json = [
            "contact_total" => $contact_number
        ];

        return response() -> json($json);
    }

}
