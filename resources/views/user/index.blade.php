<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Usuarios</title>

    <!-- Css -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Js -->
</head>
<body>
    <div class="container">
        <div class="row">
            <h1>Lista de contactos</h1>
        </div>
        <div class="row">
            <ul>
                <li>Contacto 1</li>
                <li>Contacto 2</li>
            </ul>
        </div>
        <div class="row">
            <button type="button" class="btn btn-primary">Exportar a salesforce</button>
        </div>
    </div>
</body>
</html>