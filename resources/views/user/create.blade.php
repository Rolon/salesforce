<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Usuarios</title>

    <!-- Css -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Js -->
</head>

<body>
    <div class="container">
        <div class="row">
            <h1>Crear contacto</h1>
        </div>
        <div class="row">
            <form action="/user" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="firstNanme">Nombres: </label>
                    <input type="text" class="form-control" id="firstNanme" aria-describedby="emailHelp"
                        placeholder="Nombres" name="firstNanme">
                </div>
                <div class="form-group">
                    <label for="lastName">Apellidos: </label>
                    <input type="text" class="form-control" id="lastName" aria-describedby="emailHelp"
                        placeholder="Apellidos" name="lastName">
                </div>
                <div class="form-group">
                    <label for="latitude">Latitud: </label>
                    <input type="text" class="form-control" id="latitude" aria-describedby="emailHelp"
                        placeholder="Latitud" name="latitude">
                </div>
                <div class="form-group">
                    <label for="length">Longitud: </label>
                    <input type="text" class="form-control" id="length" aria-describedby="emailHelp"
                        placeholder="Longitud" name="length">
                </div>
                <div class="form-group">
                    <label for="device">Dispositivo: </label>
                    <input type="text" class="form-control" id="device" aria-describedby="emailHelp"
                        placeholder="Dispositivo" name="device">
                </div>
                <div class="form-group">
                    <label for="number">Teléfono: </label>
                    <input type="text" class="form-control" id="number" aria-describedby="emailHelp"
                        placeholder="Teléfono" name="number">
                </div>
                <div class="form-group">
                    <label for="pathPhoto">Foto: </label>
                    <input type="file" class="form-control" id="pathPhoto" aria-describedby="emailHelp"
                        placeholder="Enter email" name="pathPhoto">
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
            </form>
        </div>
    </div>
</body>

</html>
