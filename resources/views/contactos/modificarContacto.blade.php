@extends('layouts.contactos_layout')

@section('title')
Modificar contacto
@endsection

@section('header')
Modificar Contacto
@endsection

@section('content')
<div class="row">
    <div class="col">
        <form action="/modificarC/{{ $contacto -> id}}" method="post">
            @csrf
            {{ method_field('PATCH') }}
            <div class="form-group">
                <label for="name">Nombre:</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ $contacto -> name }}" autocomplete="off">
            </div>
            <div class="form-group">
                <label for="lastName">Apellidos:</label>
                <input type="text" id="lastName" name="lastName" class="form-control" value="{{ $contacto -> lastName }}" autocomplete="off">
            </div>
            <div class="form-group">
                <label for="mail">Correo Electronico:</label>
                <input type="mail" id="mail" name="mail" class="form-control" value="{{ $contacto -> correo }}" autocomplete="off">
            </div>
            <div class="form-group">
                <label for="genero">Genero:</label>
                <select name="genero" id="genero" class="form-control">
                    <option value="0" selected>Seleccione opción</option>
                    <option value="male" @if($contacto -> genero == 'male' ) selected @endif>male</option>
                    <option value="female" @if($contacto -> genero == 'female' ) selected @endif>female</option>
                </select>
            </div>
            <div class="form-group">
                <label for="mobile">Télefono:</label>
                <input type="number" id="mobile" name="mobile" class="form-control" value="{{ $contacto -> mobile }}" autocomplete="off">
            </div>            
            <input class="btn btn-primary" type="submit" value="Guardar Datos">
        </form>
    </div>
</div>
@endsection
