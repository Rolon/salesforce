@extends('layouts.contactos_layout')



@section('title')

Buscar contacto

@endsection



@section('header')

Buscar contacto de salesforce

@endsection



@section('content')

<div class="row" id="findByMobile">

    <form action="/findContactByNumber" method="GET">

        <div class="form-group">

            <label for="mobile">Mobile Phone</label>

            <input type="number" class="form-control" id="mobile" name="mobile" aria-describedby="mobile" placeholder="Enter mobile phone">

        </div>

        <button type="submit" class="btn btn-primary">Submit</button>

    </form>

</div>

<div class="row" id="findByName">



</div>

<div class="row" id="findBy">



</div>

<div class="row" id="data">
	@if(isset($contact))
		<ul class="list-group">
    			<li class="list-group-item">Nombre del contacto: {{ $contact -> Name }}</li>
    			<li class="list-group-item">Número Celular: {{ $contact -> Phone }}</li>
    			<li class="list-group-item">Email {{ $contact -> Email }}</li>
        	</ul>	
	@endif
</div>

@endsection

