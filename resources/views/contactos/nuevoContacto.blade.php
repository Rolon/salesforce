@extends('layouts.contactos_layout')

@section('title')
Crear contacto
@endsection

@section('header')
Nuevo Contacto
@endsection

@section('content')
<div class="row">
    <div class="col">
        <form action="/createC" method="post">
            @csrf
            <div class="form-group">
                <label for="name">Nombre:</label>
                <input type="text" id="name" name="name" class="form-control" autocomplete="off">
            </div>
            <div class="form-group">
                <label for="lastName">Apellidos:</label>
                <input type="text" id="lastName" name="lastName" class="form-control" autocomplete="off">
            </div>
            <div class="form-group">
                <label for="mail">Correo Electronico:</label>
                <input type="mail" id="mail" name="mail" class="form-control" value="" autocomplete="off">
            </div>
            <div class="form-group">
                <label for="genero">Genero:</label>
                <select name="genero" id="genero" class="form-control">
                    <option value="0" selected>Seleccione opción</option>
                    <option value="male" >male</option>
                    <option value="female" >female</option>
                </select>
            </div>
            <div class="form-group">
                <label for="mobile">Télefono:</label>
                <input type="number" id="mobile" name="mobile" class="form-control" autocomplete="off">
            </div>            
            <input class="btn btn-primary" type="submit" value="Guardar Datos">
        </form>
    </div>
</div>
@endsection
