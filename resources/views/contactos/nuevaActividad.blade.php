@extends('layouts.contactos_layout')

@section('title')
Crear Actividad
@endsection

@section('header')
Nueva Actividad
@endsection

@section('content')
<div class="row">
    <div class="col">
        <form action="/createA" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="contacto">Contacto:</label>
                <select name="contacto" id="contacto" class="form-control">
                    @foreach($contactos as $contacto) 
                        <option value="{{ $contacto -> id }}">{{ $contacto -> name }} {{ $contacto -> lastName }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="photo">Foto:</label>
                <input type="file" id="photo" name="photo" class="form-control" autocomplete="off">
            </div>
            <div class="form-group">
                <label for="device">Dispositivo:</label>
                <select name="device" id="device" class="form-control">
                    @foreach($dispositivos as $dispositivo) 
                        <option value="{{ $dispositivo -> id }}">{{ $dispositivo -> device }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="georeference">Georeferencia:</label>
                <input type="text" id="georeference" name="georeference" class="form-control" autocomplete="off">
            </div>
            <div class="form-group">
                <label for="description">Actividad:</label>
                <textarea class="form-control" rows="4" cols="50" id="description" name="description"></textarea>
            </div>
            <input class="btn btn-primary" type="submit" value="Guardar Datos">
        </form>
    </div>
</div>
@endsection
