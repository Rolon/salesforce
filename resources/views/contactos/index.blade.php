@extends('layouts.contactos_layout')

@section('title')
 Ver contactos
@endsection

@section('header')
Contactos de salesforce
@endsection

@section('content')
    <ul class="list-group">
        @for($i = 0; $i < count($contacts) ; $i++)
            <li class="list-group-item"><a href="contactos/{{ $contacts[$i] -> Id }}">{{ $contacts[$i] -> Name }}</a></li>
        @endfor
    </ul>
@endsection