@extends('layouts.contactos_layout')

@section('title')
Exportar contactos
@endsection

@section('header')
Exportar contactos
@endsection

@section('content')
@if(isset($contactos))
<div class="row">
    <div class="col">
        <h5>Sincronizar todos los contactos</h5>
    </div>
</div>
<div class="row">
    <div class="col">
        <!-- Button trigger modal -->
        <button id="sincronizar" class="btn btn-primary">Sincronizar contactos</button>
    </div>
</div>
<div class="row">&nbsp;</div>
<div class="row">
    <div class="col">
        <h5>Elija los contactos que desa exportar</h5>
    </div>
</div>
<div class="row">&nbsp;</div>
<div class="row">
    <div class="col">
        <form action="/exportContacts" method="POST">
            @csrf
            <input class="btn btn-secondary" type="submit" value="Exportar contactos seleccionados">
            @foreach($contactos as $contacto)
            <div class="form-check">
                <input type="checkbox" class="form-check-input" name="contacto[]" id="contacto"
                    value="{{ $contacto -> id }}"> {{ $contacto -> name }} {{ $contacto -> lastname }}
            </div>
            @endforeach
        </form>
    </div>
</div>
@else
<div class="row">
    <div class="col">
        <p>Inserte más contactos para continuar con su exportación</p>
    </div>
</div>
@endif

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Exportando contactos</h5>
            </div>
            <div class="modal-body">
                <p>Este proceso puede tardar muchos minutos...</p>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped bg-info progress-bar-animated" role="progressbar"
                        aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    // Boton para sincronizar contacto
    var sincronizar = document.getElementById("sincronizar");

    sincronizar.addEventListener("click", sendRequest);

    function sendRequest() {
        console.log("click");

        var modal = document.getElementById("exampleModal");

        modal.style.display = "block";
        modal.classList.add("show");
        document.body.innerHTML += '<div id="backgroundModal" class="modal-backdrop fade show"></div>';

        let url = '/sincronizar';
        fetch(url, {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json, text-plain, */*",
                    "X-Requested-With": "XMLHttpRequest",
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                method: 'post',
                credentials: "same-origin",
                body: JSON.stringify({
                    export: true
                })
            })
            .then(function (response) {
                console.log(response.json());
                var modal = document.getElementById("exampleModal");
                modal.style.display = "none";
                modal.classList.remove("show");
                var element = document.getElementById("backgroundModal");
                element.parentNode.removeChild(element);
                //location.reload();
            })
    }

</script>
@endsection
