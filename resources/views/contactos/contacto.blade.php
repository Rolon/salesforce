@extends('layouts.contactos_layout')

@section('title')
 Ver contacto
@endsection

@section('header')
Datos del contacto
@endsection

@section('content')
    <ul class="list-group">
    <li class="list-group-item">Nombre del contacto: {{ $data_contact -> fullName }}</li>
    <li class="list-group-item">Número Celular: {{ $data_contact -> mobilePhone }}</li>
    <li class="list-group-item">Número de Casa: {{ $data_contact -> homePhone }}</li>
    <li class="list-group-item">Otro número: {{ $data_contact -> otherPhone }}</li>
    <li class="list-group-item">Actividad: {{ $data_contact -> description }}</li>
    <li class="list-group-item">Fecha de captura: {{ $data_contact -> createdDate }}</li>
    </ul>
@endsection