@extends('layouts.contactos_layout')

@section('title')
Salesforce Config
@endsection

@section('header')
SalesForce Configuration
@endsection

@section('content')
<div class="row">
    <div class="col">
        <h5>Estado de conexión con salesforce</h5>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="progress">
            <div id="verifyConnection" class="progress-bar progress-bar-striped bg-primary progress-bar-animated"
                role="progressbar" style="width: 0%"></div>
        </div>
    </div>
    <div class="col d-flex justify-content-center">
        <a href="/revoke" class="btn btn-danger">Desconectar de salesforce</a>
        &nbsp;&nbsp;
        <a href="/refreshToken" class="btn btn-dark">Reconectar con Token</a>
        &nbsp;&nbsp;
        <a href="/refreshUser" class="btn btn-primary">Reconectar con Usuario</a>
    </div>
</div>
<div class="row">
    <div class="col">
        <h5><span id="textVerify" class="badge badge-primary">0% - No conectado</span></h5>
    </div>
</div>
<div class="row">
    &nbsp;
</div>
<div class="row">
    <div class="col">
        <div class="progress" style="height: 1px;">
            <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
    </div>
</div>
<div class="row">
    &nbsp;
</div>
<div class="row">
    <div class="col">
        <h5>Configurar datos de salesforce</h5>
    </div>
</div>
<div class="row">
    <div class="col">
        <select id="selectLogin">
            <option value="1" selected>Config Login By User</option>
            <option value="2">Config Login By Token</option>
        </select>
    </div>
</div>
<div class="row">&nbsp;</div>
<div class="row" id="loginByUser">
    <div class="col">
        <form action="/salesforceConfigByUser" method="POST">
            @csrf
            <div class="form-group">
                <label for="user">User:</label>
                <input type="text" id="user" name="user" class="form-control" value="{{ $json_env -> user }}" required
                    autocomplete="off">
            </div>
            <div class="form-group">
                <label for="pass">Password:</label>
                <input type="password" id="pass" name="pass" class="form-control" value="{{ $json_env -> pass }}"
                    required autocomplete="off">
            </div>
            <div class="form-group">
                <label for="keyOne">Consumer Key:</label>
                <input type="password" id="keyOne" name="keyOne" class="form-control" value="{{ $json_env -> key }}"
                    required autocomplete="off">
            </div>
            <div class="form-group">
                <label for="keyTwo">Consumer secret:</label>
                <input type="password" id="keyTwo" name="keyTwo" class="form-control" value="{{ $json_env -> secret }}"
                    required autocomplete="off">
            </div>
            <input class="btn btn-primary" type="submit" value="Guardar Datos">
        </form>
    </div>
</div>
<div class="row" id="loginByToken">
    <div class="col">
        <form action="/salesforceConfigByToken" method="POST">
            @csrf
            <div class="form-group">
                <label for="keyOne">Consumer Key:</label>
                <input type="password" id="keyOne" name="keyOne" class="form-control" value="{{ $json_env -> key }}"
                    required autocomplete="off">
            </div>
            <div class="form-group">
                <label for="keyTwo">Consumer secret:</label>
                <input type="password" id="keyTwo" name="keyTwo" class="form-control" value="{{ $json_env -> secret }}"
                    required autocomplete="off">
            </div>
            <input class="btn btn-primary" type="submit" value="Guardar Datos">
        </form>
    </div>
</div>
<div class="row">
    &nbsp;
</div>
@endsection

@section('script')
<!-- Script para cambiar el tipo de login -->
<script>
    // Select
    var select = document.getElementById("selectLogin");

    // Forms
    var formUser = document.getElementById("loginByUser");
    var formToken = document.getElementById("loginByToken");

    // Hides token login
    formToken.style.display = "none";

    // Listener
    select.addEventListener("change", changeLogin);

    // Change the way to config login
    function changeLogin() {
        if (select.value == 1) {
            formToken.style.display = "none";
            formUser.style.display = "flex";
        } else {
            formUser.style.display = "none";
            formToken.style.display = "flex";
        }
    }

</script>

<!-- Script para verificar la conexión con salesforce -->
<script>
    try {
        /* Primero verifica si existen los datos del usuario */
        let url = "/verifyData";
        fetch(url, {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json, text-plain, */*",
                    "X-Requested-With": "XMLHttpRequest",
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                method: 'post',
                credentials: "same-origin",
                body: JSON.stringify({
                    verify: true
                })
            })
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                let progressBar = document.getElementById("verifyConnection");
                let textVerify = document.getElementById("textVerify");

                let dataset = data.data_set;

                if (dataset) {
                    progressBar.style.width = "50%";
                    textVerify.textContent = "50% - Conectando con salesforce";

                    progressBar.classList.remove("bg-primary");
                    progressBar.classList.add("bg-info");
                    textVerify.classList.remove("badge-primary");
                    textVerify.classList.add("badge-info");
                } else {
                    progressBar.style.width = "0%";
                    textVerify.textContent = "0% - No conectado";
                }
            })

        url = "/verifyConnection";
        fetch(url, {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json, text-plain, */*",
                    "X-Requested-With": "XMLHttpRequest",
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                method: 'post',
                credentials: "same-origin",
                body: JSON.stringify({
                    verify: true
                })
            })
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                let active = data.active;
                let progressBar = document.getElementById("verifyConnection");
                let textVerify = document.getElementById("textVerify");

                if (active) {
                    progressBar.style.width = "100%";
                    textVerify.textContent = "100% - Conectado con salesforce";

                    progressBar.classList.remove("bg-info");
                    progressBar.classList.remove("progress-bar-animated");
                    progressBar.classList.remove("progress-bar-striped");
                    progressBar.classList.add("bg-success");
                    textVerify.classList.remove("badge-info");
                    textVerify.classList.add("badge-success");
                } else {
                    if (progressBar.style.width === "0%") {
                        textVerify.textContent = "0% - Realice el login con salesforce";

                        progressBar.classList.remove("bg-info");
                        progressBar.classList.add("bg-danger");
                        textVerify.classList.remove("badge-info");
                        textVerify.classList.add("badge-danger");
                    } else {
                        textVerify.textContent = "50% - Realice el login con salesforce";

                        progressBar.classList.remove("bg-info");
                        progressBar.classList.add("bg-warning");
                        textVerify.classList.remove("badge-info");
                        textVerify.classList.add("badge-warning");
                    }
                }
            })
    } catch (error) {
        console.error(error);
    }

</script>
@endsection
