@extends('layouts.contactos_layout')

@section('title')
Modificar Contactos
@endsection

@section('header')
Seleccione un contacto para modificar
@endsection

@section('content')
<div class="row">
    <div class="col">
        <ul>
            @foreach($contactos as $contacto)
                <li><a href="/modificarContacto/{{ $contacto -> id }}">{{ $contacto -> name }} {{ $contacto -> lastName }}</a></li>
            @endforeach
        </ul>
    </div>
</div>
@endsection
