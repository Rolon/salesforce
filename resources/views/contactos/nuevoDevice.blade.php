@extends('layouts.contactos_layout')

@section('title')
Crear Dispositivo
@endsection

@section('header')
Nuevo Dispositivo
@endsection

@section('content')
<div class="row">
<div class="col">
        <form action="/createD" method="post">
            @csrf
            <div class="form-group">
                <label for="nameD">Nombre del Dispositivo:</label>
                <input type="text" id="nameD" name="nameD" class="form-control" autocomplete="off">
            </div>    
            <input class="btn btn-primary" type="submit" value="Guardar Dispositivo">
        </form>
    </div>
</div>
@endsection
