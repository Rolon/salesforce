@extends('errors.illustrated-layout')

@section('title', __('Salesforce Response Error Connection'))
@section('code', 'Error')

@section('message')
    Compruebe lo siguiente antes de conectarse con salesforce
    <ul>
        <li>Sus datos de conexion estan biene escritos</li>
        <li>Su cuenta es compatible para el uso de aplicaciones remotas</li>
        <li>Es posible que no haya pasado el tiempo suficiente para regristrar la aplicación</li>
        <li>Verifique las politicas de conexión de su aplicación</li>
    </ul>
@endsection
