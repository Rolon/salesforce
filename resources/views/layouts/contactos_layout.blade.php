<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>@yield('title')</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>    

</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <h1>@yield('header')</h1>
                <p>
                    <a href="../">Volver al inicio</a>
                </p>
            </div>
        </div>
        <div class="row" id="notification">

        </div>
        @yield('content')
    </div>
</body>
@yield('script')
<script src="{{ asset('js/synchronize.js') }}" defer></script>
</html>
