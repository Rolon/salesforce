<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Salesforce Test</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <h1>Salesforce Test</h1>
            </div>
        </div>
        <div class="row" id="notification">

        </div>
        <div class="row">
            <div class="col">
                <h3>Test Links</h3>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <ul>
                    <li><a href="/contactoNV">Crear contacto</a></li>
                    <li><a href="/ActividadNV">Crear Actividad</a></li>
                    <li><a href="/DispositivoNV">Crear Dispositivo</a></li>
                    <li><a href="/modificarContacto">Modificar contactos</a></li>
                    <li><a href="/config">Configurar Salesforce</a></li>
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</body>

<script src="{{ asset('js/synchronize.js') }}" defer></script>

</html>
